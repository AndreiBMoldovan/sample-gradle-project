package com.example;

import com.example.ui.CalculatorConsoleUI;

public class Main {
    public static void main(String[] args) {
        CalculatorConsoleUI ui = new CalculatorConsoleUI();

        ui.run();
    }
}