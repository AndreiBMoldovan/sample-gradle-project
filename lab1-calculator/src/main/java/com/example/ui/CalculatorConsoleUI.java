package com.example.ui;

import com.example.exceptions.DivisionByZeroException;
import com.example.services.Calculator;

import java.util.Objects;
import java.util.Scanner;

public class CalculatorConsoleUI {
    private final Calculator calculator;

    private final Scanner scanner;

    public CalculatorConsoleUI() {
        this.calculator = new Calculator();
        this.scanner = new Scanner(System.in);
    }

    private String getAvailableOperations() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Available operations are:\n");
        stringBuilder.append("Add -> +\n");
        stringBuilder.append("Subtract -> -\n");
        stringBuilder.append("Multiply -> *\n");
        stringBuilder.append("Divide -> /\n");
        stringBuilder.append("Min -> min\n");
        stringBuilder.append("Max -> max\n");
        stringBuilder.append("Sqrt -> sqrt\n");
        stringBuilder.append("Reset -> AC\n");
        stringBuilder.append("Close -> exit\n");

        return stringBuilder.toString();
    }

    private String generateGreetMessages() {
        return "**** Welcome to top notch 1970 calculator ****";
    }

    private String getUserInputOperationMessage() {
        return "Operation is: ";
    }

    private String getUserInputValueMessage() {
        return "Enter a value: ";
    }

    private String getCurrentValueMessage() {
        return "Current value: " + this.calculator.getCurrentValue();
    }

    private Double getUserInputValue(String operation) {
        System.out.print(this.getUserInputValueMessage());

        if (Objects.equals(operation, "-") && !this.scanner.hasNextDouble()) {
            return (double) 1;
        }

        if (!this.scanner.hasNextDouble()) {
            return (double) 0;
        }

        return this.scanner.nextDouble();
    }

    private void handleAdditionOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.addValue(readValue);
    }

    private void handleSubtractionOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.subtractValue(readValue);
    }

    private void handleMultiplicationOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.multiplyValue(readValue);
    }

    private void handleDivisionOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);

        try {
            this.calculator.divideValue(readValue);
        } catch (DivisionByZeroException e) {
            System.out.println(e.getMessage());
        }
    }

    private void handleMinOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.minValue(readValue);
    }

    private void handleMaxOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.maxValue(readValue);
    }

    private void handleSqrtOperation() {
        this.calculator.sqrtValue();
    }

    private void handleResetOperation() {
        this.calculator.resetValue();
    }

    public void run() {
        String greetMessages = this.generateGreetMessages();
        String availableOperations = this.getAvailableOperations();

        System.out.println(greetMessages);
        System.out.println(availableOperations);

        String operation;
        boolean runningStatus = true;

        while (runningStatus) {
            String currentValueMessage = this.getCurrentValueMessage();
            System.out.println(currentValueMessage);

            System.out.print(this.getUserInputOperationMessage());
            operation = this.scanner.nextLine();

            if (Objects.equals(operation, "exit")) {
                runningStatus = false;

                continue;
            }

            switch (operation) {
                case "+" : handleAdditionOperation(operation); break;
                case "-" : handleSubtractionOperation(operation); break;
                case "*" : handleMultiplicationOperation(operation); break;
                case "/" : handleDivisionOperation(operation); break;
                case "min" : handleMinOperation(operation); break;
                case "max" : handleMaxOperation(operation); break;
                case "sqrt" : handleSqrtOperation(); break;
                case "AC" : handleResetOperation(); break;
                default : continue;
            }
        }
    }
}
