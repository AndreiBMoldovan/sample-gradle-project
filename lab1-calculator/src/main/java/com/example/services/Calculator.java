package com.example.services;

import com.example.exceptions.DivisionByZeroException;

public class Calculator {
    private Double currentValue;

    public Calculator() {
        this.currentValue = (double) 0;
    }

    public void addValue(Double value) {
        this.currentValue += value;
    }

    public void subtractValue(Double value) {
        this.currentValue -= value;
    }

    public void multiplyValue(Double value) {
        this.currentValue *= value;
    }

    public void divideValue(Double value) throws DivisionByZeroException {
        if (value == 0) {
            throw new DivisionByZeroException("Encountered division by zero");
        }

        this.currentValue /= value;
    }

    public void minValue(Double value) {
        this.currentValue = Math.min(this.currentValue, value);
    }

    public void maxValue(Double value) {
        this.currentValue = Math.max(this.currentValue, value);
    }

    public void sqrtValue() {
        this.currentValue = Math.sqrt(this.currentValue);
    }

    public void resetValue() {
        this.currentValue = (double) 0;
    }

    public Double getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(Double currentValue) {
        this.currentValue = currentValue;
    }
}
